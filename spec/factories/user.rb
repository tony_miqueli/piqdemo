FactoryBot.define do
  factory :user do
    sequence(:email) { |n| 'user%s@example.com' % n }
    password {'password'}
  end
end