FactoryBot.define do
  factory :employee do
    sequence(:name) { |n| 'Employee %s' % n }
    address_street { 'street address' }
    phone_home { '000.000.0000' }
    title { 'Job Title' }
    salary { 10 }
    bonus { 1 }
  end
end