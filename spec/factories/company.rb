FactoryBot.define do
  factory :company do
    sequence(:name) { |n| 'Company %s' % n }
  end
end