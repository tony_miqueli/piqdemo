FactoryBot.define do
  factory :department do
    sequence(:name) { |n| 'Department %s' % n }
  end
end