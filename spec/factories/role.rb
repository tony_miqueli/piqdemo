FactoryBot.define do
  factory :role do
      trait :administrator do
        name { 'Administrator' }
      end
      trait :employee do
        name { 'Employee' }
      end      
  end
end