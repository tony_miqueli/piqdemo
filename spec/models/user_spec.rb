require 'rails_helper'

describe 'User' do

  let(:user) { FactoryBot.create(:user) }

  it 'should be valid' do
    expect(user.valid?).to eq true
  end

  it 'should be invalid if missing email' do 
    user.email = nil
    expect(user.valid?).to eq false
  end

  it 'should be invalid if email malformed' do 
    user.email = "not an email"
    expect(user.valid?).to eq false
  end

  it 'should be invalid if missing password' do 
    user.password = nil
    expect(user.valid?).to eq false
  end  

end
