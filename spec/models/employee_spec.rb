require 'rails_helper'

describe 'Employee' do

  let!(:company) { FactoryBot.create :company }
  let!(:department) { FactoryBot.create :department, company: company }
  let!(:role) { FactoryBot.create :role, :employee }
  let!(:user) { FactoryBot.create :user }
  let!(:employee) { FactoryBot.create :employee, department: department, role: role, user: user }

  it 'should be valid' do
    expect(employee.valid?).to eq true
  end

  it 'should be invalid if missing User' do 
    employee.user = nil
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if missing Department' do 
    employee.department = nil
    expect(employee.valid?).to eq false
  end 

  it 'should be invalid if missing Role' do 
    employee.role = nil
    expect(employee.valid?).to eq false
  end   

  it 'should be invalid if missing name' do 
    employee.name = nil
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if missing address_street' do 
    employee.address_street = nil
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if missing phone' do 
    employee.phone_home = nil
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if malformed phone' do 
    employee.phone_home = "not a phone number"
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if missing title' do 
    employee.title = nil
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if missing salary' do 
    employee.salary = nil
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if salary less than 0' do 
    employee.salary = -1
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if salary not an integer' do 
    employee.salary = "not a salary"
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if missing bonus' do 
    employee.bonus = nil
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if bonus less than 0' do 
    employee.bonus = -1
    expect(employee.valid?).to eq false
  end

  it 'should be invalid if bonus not an integer' do 
    employee.bonus = "not a bonus"
    expect(employee.valid?).to eq false
  end

end
