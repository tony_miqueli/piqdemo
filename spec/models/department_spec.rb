require 'rails_helper'

describe 'Department' do

  let(:company) { FactoryBot.create(:company) }
  let(:department) { FactoryBot.create(:department, company: company) }

  it 'should be valid' do
    expect(department.valid?).to eq true
  end

  it 'should be invalid if missing name' do 
    department.name = nil
    expect(department.valid?).to eq false
  end

  it 'should be invalid if missing Company' do 
    department.company = nil
    expect(department.valid?).to eq false
  end  

  it 'should be invalid if duplicate name for same Company' do 
    new_department = Department.new(name: department.name, company: company)
    expect(new_department.valid?).to eq false
  end

end
