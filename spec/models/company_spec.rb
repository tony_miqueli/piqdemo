require 'rails_helper'

describe 'Company' do

  let(:company) { FactoryBot.create(:company) }

  it 'should be valid' do
    expect(company.valid?).to eq true
  end

  it 'should be invalid if missing name' do 
    company.name = nil
    expect(company.valid?).to eq false
  end

  it 'should be invalid if duplicate name' do 
    new_company = Company.new(name: company.name)
    expect(new_company.valid?).to eq false
  end

end
