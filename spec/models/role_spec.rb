require 'rails_helper'

describe 'Role' do

  let(:role) { FactoryBot.create(:role, :administrator) }

  it 'should be valid' do
    expect(role.valid?).to eq true
  end

  it 'should be invalid if missing name' do 
    role.name = nil
    expect(role.valid?).to eq false
  end

  it 'should be invalid if duplicate name' do 
    new_role = Role.new(name: role.name)
    expect(new_role.valid?).to eq false
  end

end
