require 'rails_helper'

RSpec.describe "Sessions" do

  it "signs user in and out" do
    company = FactoryBot.create :company
    department = FactoryBot.create :department, company: company
    role = FactoryBot.create :role, :administrator
    user = FactoryBot.create :user, email: "user@example.org", password: "very-secret"
    employee = FactoryBot.create :employee, department: department, role: role, user: user

    sign_in user
    get root_path
    expect(controller.current_user).to eq(user)

    sign_out user
    get root_path
    expect { controller.current_user }.to raise_error(NoMethodError)

  end

end


