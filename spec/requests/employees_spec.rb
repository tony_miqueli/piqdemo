require 'rails_helper'

RSpec.describe "Employees", type: :request do

  before do
    @companyA = FactoryBot.create :company, name: "Company A"
    @companyB = FactoryBot.create :company, name: "Company B"
    
    @department1 = FactoryBot.create :department, company: @companyA
    @department2 = FactoryBot.create :department, company: @companyB

    @roleAdmin = FactoryBot.create :role, :administrator
    @roleEmployee = FactoryBot.create :role, :employee
    
    @userCompanyAAdmin = FactoryBot.create :user
    @employeeCompanyAAdmin = FactoryBot.create :employee, department: @department1, role: @roleAdmin, user: @userCompanyAAdmin

    @userCompanyAEmployee = FactoryBot.create :user
    @employeeCompanyAEmployee = FactoryBot.create :employee, department: @department1, role: @roleEmployee, user: @userCompanyAEmployee    

    @userCompanyBAdmin = FactoryBot.create :user
    @employeeCompanyBAdmin = FactoryBot.create :employee, department: @department2, role: @roleAdmin, user: @userCompanyBAdmin

    @userCompanyBEmployee = FactoryBot.create :user
    @employeeCompanyBEmployee = FactoryBot.create :employee, department: @department2, role: @roleEmployee, user: @userCompanyBEmployee       
  
  end

  describe 'signed in company Admin' do
  
    before do 
      sign_in @userCompanyAAdmin
    end

    after do
      sign_out @userCompanyAAdmin
    end

    it "Admin has access to own employee page" do
      get employee_path(@userCompanyAAdmin.employee)
      expect(response.code).to eq '200'
    end

    it "Admin has access to own edit employee page" do
      get edit_employee_path(@userCompanyAAdmin.employee)
      expect(response.code).to eq '200'
    end

    it "Admin can successfully update own employee page" do
      put(employee_path(@userCompanyAAdmin.employee), params: {"employee" => {:name => "new name"}})
      @userCompanyAAdmin.employee.reload
      expect(@userCompanyAAdmin.employee.name).to eq 'new name'
    end        

    it "Admin can successfully update own compensation employee page" do
      salary = @userCompanyAAdmin.employee.salary
      put(employee_path(@userCompanyAAdmin.employee), params: {"employee" => {:salary => salary + 1000}})
      @userCompanyAAdmin.employee.reload
      expect(@userCompanyAAdmin.employee.salary).to eq (salary + 1000)
    end    

    it "Admin has access to other company employee page" do
      get employee_path(@userCompanyAEmployee.employee)
      expect(response.code).to eq '200'
    end

    it "Admin has access to other company edit employee page" do
      get edit_employee_path(@userCompanyAEmployee.employee)
      expect(response.code).to eq '200'
    end

    it "Admin can successfully update other company employee page" do
      put(employee_path(@userCompanyAEmployee.employee), params: {"employee" => {:name => "new name"}})
      @userCompanyAEmployee.employee.reload
      expect(@userCompanyAEmployee.employee.name).to eq 'new name'
    end        

    it "Admin can successfully update other company compensation employee page" do
      salary = @userCompanyAEmployee.employee.salary
      put(employee_path(@userCompanyAEmployee.employee), params: {"employee" => {:salary => salary + 1000}})
      @userCompanyAEmployee.employee.reload
      expect(@userCompanyAEmployee.employee.salary).to eq (salary + 1000)
    end     

  end

  describe 'signed in company Employee' do
    before do 
      sign_in @userCompanyAEmployee
    end

    after do
      sign_out @userCompanyAEmployee
    end  

    it "Employee has access to own employee page" do
      get employee_path(@userCompanyAEmployee.employee)
      expect(response.code).to eq '200'
    end

    it "Employee has access to own edit employee page" do
      get edit_employee_path(@userCompanyAEmployee.employee)
      expect(response.code).to eq '200'
    end

    it "Employee can successfully update own employee page" do
      put(employee_path(@userCompanyAEmployee.employee), params: {"employee" => {:name => "new name"}})
      @userCompanyAEmployee.employee.reload
      expect(@userCompanyAEmployee.employee.name).to eq 'new name'
    end        

    it "Employee CANNOT successfully update own compensation employee page" do
      salary = @userCompanyAEmployee.employee.salary
      put(employee_path(@userCompanyAEmployee.employee), params: {"employee" => {:salary => salary + 1000}})
      @userCompanyAEmployee.employee.reload
      expect(@userCompanyAEmployee.employee.salary).to eq salary
    end    

    it "Employee CANNOT access other company employee page" do
      get employee_path(@userCompanyBEmployee.employee)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Employee CANNOT access other company edit employee page" do
      get edit_employee_path(@userCompanyBEmployee.employee)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Employee CANNOT update other company employee page" do
      name = @userCompanyBEmployee.employee.name
      put(employee_path(@userCompanyBEmployee.employee), params: {"employee" => {:name => "new name"}})
      @userCompanyBEmployee.employee.reload
      expect(@userCompanyBEmployee.employee.name).to eq name
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end        

    it "Employee CANNOT update other company compensation employee page" do
      salary = @userCompanyBEmployee.employee.salary
      put(employee_path(@userCompanyBEmployee.employee), params: {"employee" => {:salary => salary + 1000}})
      @userCompanyBEmployee.employee.reload
      expect(@userCompanyBEmployee.employee.salary).to eq salary
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end   

  end


end