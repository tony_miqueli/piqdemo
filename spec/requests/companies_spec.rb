require 'rails_helper'

RSpec.describe "Companies", type: :request do

  before do
    @companyA = FactoryBot.create :company, name: "Company A"
    @companyB = FactoryBot.create :company, name: "Company B"
    
    @department1 = FactoryBot.create :department, company: @companyA
    @department2 = FactoryBot.create :department, company: @companyB

    @roleAdmin = FactoryBot.create :role, :administrator
    @roleEmployee = FactoryBot.create :role, :employee
    
    @userCompanyAAdmin = FactoryBot.create :user
    @employeeCompanyAAdmin = FactoryBot.create :employee, department: @department1, role: @roleAdmin, user: @userCompanyAAdmin

    @userCompanyAEmployee = FactoryBot.create :user
    @employeeCompanyAEmployee = FactoryBot.create :employee, department: @department1, role: @roleEmployee, user: @userCompanyAEmployee    

    @userCompanyBAdmin = FactoryBot.create :user
    @employeeCompanyBAdmin = FactoryBot.create :employee, department: @department2, role: @roleAdmin, user: @userCompanyBAdmin

    @userCompanyBEmployee = FactoryBot.create :user
    @employeeCompanyBEmployee = FactoryBot.create :employee, department: @department2, role: @roleEmployee, user: @userCompanyBEmployee       
  
  end

  describe 'signed in company Admin' do
  
    before do 
      sign_in @userCompanyAAdmin
    end

    after do
      sign_out @userCompanyAAdmin
    end

    it "Admin has access to Company profile page" do
      get company_path(@userCompanyAAdmin.company)
      expect(response.code).to eq '200'
    end

    it "Admin has access to Company edit page" do
      get edit_company_path(@userCompanyAAdmin.company)
      expect(response.code).to eq '200'
    end

    it "Admin can successfully post to Company update page" do
      put(company_path(@userCompanyAAdmin.company), params: {"company" => {:name => "new name"}})
      @userCompanyAAdmin.company.reload
      expect(@userCompanyAAdmin.company.name).to eq 'new name'
    end    

    it "Admin has access to Company Employees page" do
      get employees_company_path(@userCompanyAAdmin.company)
      expect(response.code).to eq '200'
    end

    it "Admin CANNOT access other Company profile page" do
      get company_path(@companyB)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Admin CANNOT access other Company edit page" do
      get edit_company_path(@companyB)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Admin CANNOT successfully post to other Company update page" do
      company_name = @companyB.name
      put(company_path(@companyB), params: {"company" => {:name => "new name"}})
      @companyB.reload
      expect(@companyB.name).to eq company_name
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'      
    end    

    it "Admin CANNOT access other Company Employees page" do
      get employees_company_path(@companyB)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

  end

  describe 'signed in company Employee' do
    before do 
      sign_in @userCompanyAEmployee
    end

    after do
      sign_out @userCompanyAEmployee
    end  

    it "Employee CANNOT access Company profile page" do
      get company_path(@userCompanyAEmployee.company)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Employee CANNOT access Company edit page" do
      get edit_company_path(@userCompanyAEmployee.company)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Employee CANNOT post to Company update page" do
      company_name = @userCompanyAEmployee.company.name  
      put(company_path(@userCompanyAEmployee.company), params: {"company" => {:name => "new name"}})
      @userCompanyAEmployee.company.reload
      expect(@userCompanyAEmployee.company.name).to eq company_name
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end    

    it "Employee CANNOT access Company Employees page" do
      get employees_company_path(@userCompanyAEmployee.company)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Employee CANNOT access other Company profile page" do
      get company_path(@companyB)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Employee CANNOT access other Company edit page" do
      get edit_company_path(@companyB)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

    it "Employee CANNOT successfully post to other Company update page" do
      company_name = @companyB.name
      put(company_path(@companyB), params: {"company" => {:name => "new name"}})
      @companyB.reload
      expect(@companyB.name).to eq company_name
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'      
    end    

    it "Employee CANNOT access other Company Employees page" do
      get employees_company_path(@companyB)
      expect(response.code).to eq '302'
      expect(controller.flash["alert"]).to eq 'Access denied.'
    end

  end


end