# README
* Ruby version 2.4.2
* PostgreSQL 10.1

# Development setup
* git clone https://tony_miqueli@bitbucket.org/tony_miqueli/piqdemo.git # or clone your own fork
* cd piqdemo
* bundle
* rake db:create
* rake db:migrate
* rake db:seed

# Test
* rspec

# Database Notes:
* Using 'Papertrail' to implement versioning.  Stores changes in table 'versions.'
* Created tables 'permissions' and 'resources' for extending roles/permissions but these aren't used in this demo.

## Heroku site:  https://piqdemo.herokuapp.com
* There are two Companies (Joe's On Broadway and Jacqueline's)
* Each company has two types of Employees ('Administrator', 'Employee')
