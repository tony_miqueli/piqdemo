class EmployeesController < ApplicationController
  
  before_action :set_employee, 
                :set_departments,
                :set_roles

  def update
    
    authorize @employee

    if @employee.update(employee_params)
      redirect_to @employee, flash: {success: "Employee successfully updated"}
    else
      render :edit 
    end
    rescue StandardError => e 
      flash.now[:error] = "Error saving Employee. #{e}"
      render :edit     
  end

  private

  def set_employee
     @employee = Employee.find(params[:id])
     authorize @employee
  end

  def set_departments
     @departments = Department.where(company_id: @employee.company)
  end

  def set_roles
    @roles = Role.all
  end

  def employee_params
    params.require(:employee).permit(policy(@employee).permitted_attributes)
  end

end


