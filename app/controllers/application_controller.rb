class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  # after_action :verify_authorized, unless: :devise_controller?

  before_action :set_paper_trail_whodunnit

  def user_for_paper_trail
    # logged_in? ? current_member.id : 'Public user'  # or whatever
  	current_user.id if current_user

  end

end
