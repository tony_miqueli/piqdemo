class CompaniesController < ApplicationController
  
  before_action :set_company

  def update
    if @company.update(company_params)
      redirect_to @company, flash: {success: "Company successfully updated"}
    else
      render :edit 
    end
    rescue StandardError => e 
      flash.now[:error] = "Error saving Company. #{e}"
      render :edit     
  end

  def employees
    @employees = @company.employees
  end

  def summary_report
    @records = @company.summary_reports.where("salary_rank <= ?", 3)
  end

  private

  def set_company
     @company = Company.find(params[:id])
     authorize @company
  end

  def company_params
    params.require(:company).permit(:name)
  end
end
