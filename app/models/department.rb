class Department < ApplicationRecord
	belongs_to :company
	has_many :employees, dependent: :destroy

	has_paper_trail :on => [:update], :skip => [:updated_at]
  strip_attributes :collapse_spaces => true

  validates :name, presence: true, uniqueness: {scope: :company}

	default_scope { includes(:company) }
end
