class Role < ApplicationRecord

	validates :name, presence: true, uniqueness: true

	def administrator?
		name == "Administrator"
	end
	
	def to_s
		name
	end
end
