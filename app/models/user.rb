class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :timeoutable, :trackable

  has_one :employee, dependent: :destroy
	delegate :company, :to => :employee, :allow_nil => true
	delegate :role, :to => :employee, :allow_nil => true
  
  default_scope { includes(:employee) }

  validates :email, uniqueness: true, presence: true, format: Devise::email_regexp
  validates :password, presence: true

  def to_login
  	"#{employee&.name} - #{company&.name}, #{employee&.role}"
  end

end
