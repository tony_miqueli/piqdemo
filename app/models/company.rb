class Company < ApplicationRecord
	has_many :departments, dependent: :destroy
	has_many :employees, -> { order 'employees.id' }, through: :departments
	has_many :summary_reports

	has_paper_trail :on => [:update], :skip => [:updated_at]
  strip_attributes :collapse_spaces => true

	validates :name, presence: true, uniqueness: true

end
