class Employee < ApplicationRecord
	belongs_to :user, dependent: :destroy
	belongs_to :department
	belongs_to :role

	delegate :company, :to => :department, :allow_nil => true

	has_paper_trail :on => [:update], :skip => [:updated_at]
  strip_attributes :collapse_spaces => true

  validates_presence_of :name, :address_street, :phone_home, :title, :salary, :bonus
 	validates_format_of :phone_home, :with => /\A\d{3}\.\d{3}\.\d{4}\Z/i
	validates_numericality_of :salary, :bonus, only_integer: true, greater_than_or_equal_to: 0, allow_blank: true


	default_scope { includes([:role, :department]) }

end
