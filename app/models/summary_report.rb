class SummaryReport < ActiveRecord::Base
  self.table_name = 'v_summary_report'

  belongs_to :company

  protected

  def readonly?
    true
  end
end 