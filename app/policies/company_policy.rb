class CompanyPolicy < ApplicationPolicy

  def show?
    isUserCompanyAdmin?
  end

  def update?
    show?
  end

  def edit?
    show?
  end

  def employees?
    show?
  end

  def summary_report?
    show?
  end

  private


  def isUserCompanyAdmin?
  	isUserCompanyEmployee? && isUserAdmin?
  end

  def isUserCompanyEmployee?
  	user.company.id == record.id
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
