class EmployeePolicy < ApplicationPolicy

  def show?
    isUserEmployee? || isUserCompanyAdmin?
  end

  def update?
    show?
  end

  def edit?
    show?
  end

  def edit_compensation?
  	isUserCompanyAdmin?
  end

  def permitted_attributes
    isUserAdmin? ? employee_params + [:department_id, :role_id, :title, :salary, :bonus] : employee_params
  end  

  private

  def employee_params
  	[:name, :address_street, :phone_home]
	end

  def isUserEmployee?
  	user.employee.id == record.id
  end

  def isUserCompanyAdmin?
  	isUserCompanyEmployee? && isUserAdmin?
  end

  def isUserCompanyEmployee?
  	user.company.id == record.company.id
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
