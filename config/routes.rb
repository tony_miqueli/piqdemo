Rails.application.routes.draw do

  #devise_for :users
  devise_for :users, controllers: { sessions: "users/sessions" }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  root 'static_pages#home'

	resources :companies, only: [:show, :edit, :update] do
	  member do
	    get :employees
	    get :summary_report
	  end
	end

	resources :employees, only: [:show, :edit, :update]

end
