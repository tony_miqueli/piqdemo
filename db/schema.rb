# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181114024717) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.bigint "company_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_departments_on_company_id"
  end

  create_table "employees", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "department_id"
    t.bigint "role_id"
    t.string "name"
    t.string "address_street"
    t.string "phone_home"
    t.string "title"
    t.integer "salary"
    t.integer "bonus"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_employees_on_department_id"
    t.index ["role_id"], name: "index_employees_on_role_id"
    t.index ["user_id"], name: "index_employees_on_user_id"
    t.index ["user_id"], name: "index_employees_on_user_id_unique", unique: true
  end

  create_table "permissions", force: :cascade do |t|
    t.bigint "role_id"
    t.bigint "resource_id"
    t.boolean "create_access", default: false
    t.boolean "read_access", default: true
    t.boolean "update_access", default: false
    t.boolean "destroy_access", default: false
    t.index ["resource_id", "role_id"], name: "index_permissions_on_resource_id_and_role_id", unique: true
    t.index ["resource_id"], name: "index_permissions_on_resource_id"
    t.index ["role_id", "resource_id"], name: "index_permissions_on_role_id_and_resource_id", unique: true
    t.index ["role_id"], name: "index_permissions_on_role_id"
  end

  create_table "resources", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.text "object_changes"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "departments", "companies"
  add_foreign_key "employees", "departments"
  add_foreign_key "employees", "roles"
  add_foreign_key "employees", "users"
  add_foreign_key "permissions", "resources"
  add_foreign_key "permissions", "roles"
end
