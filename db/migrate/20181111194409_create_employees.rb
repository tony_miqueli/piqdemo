class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
    	t.references :user, foreign_key: { to_table: :users }
    	t.references :department, foreign_key: { to_table: :departments }
    	t.references :role, foreign_key: { to_table: :roles }    	
      t.string :name
      t.string :address_street
      t.string :phone_home
      t.string :title
      t.integer :salary
      t.integer :bonus

      t.timestamps
    end
    add_index :employees, [:user_id], :unique => true, :name =>"index_employees_on_user_id_unique"
  end
end
