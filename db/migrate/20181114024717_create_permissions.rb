class CreatePermissions < ActiveRecord::Migration[5.1]
  def change
    create_table :permissions do |t|
    	t.references :role, foreign_key: { to_table: :roles }
    	t.references :resource, foreign_key: { to_table: :resources }
	    t.boolean "create_access", default: false
	    t.boolean "read_access", default: true
	    t.boolean "update_access", default: false
	    t.boolean "destroy_access", default: false   	
    end
    add_index :permissions, [:resource_id, :role_id], :unique => true, :name =>"index_permissions_on_resource_id_and_role_id"
    add_index :permissions, [:role_id, :resource_id], :unique => true, :name =>"index_permissions_on_role_id_and_resource_id"
  end
end



    
