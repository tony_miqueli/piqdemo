class CreateDepartments < ActiveRecord::Migration[5.1]
  def change
    create_table :departments do |t|
    	t.references :company, foreign_key: { to_table: :companies }
      t.string :name

      t.timestamps
    end
  end
end
