class CreateViewSummaryReport < ActiveRecord::Migration[5.1]
  def change
		self.connection.execute %Q( CREATE OR REPLACE VIEW v_summary_report AS 
			select 
			    dept.company_id,
			    emp.id employee_id,
			    dept.name department_name,
			    emp.name employee_name,
			    emp.salary,
			    rank() OVER (PARTITION BY dept.company_id, dept.name ORDER BY salary DESC) salary_rank
			from employees emp 
				inner join departments dept on emp.department_id = dept.id;) 
  end
end
