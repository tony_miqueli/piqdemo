# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Roles
['Administrator', 'Employee'].each{ |r| Role.create!(name: r) }

#Companies
["Joe's On Broadway", "Jacqueline's"].each{ |c| Company.create!(name: c) }

#Departments
Department.create!(company: Company.first, name: "Mining").tap do |dept|
	Employee.create!(user: User.new(email: 'samual@hahnharber.name', password: 'password'), phone_home: '372.812.8272', bonus: 4023, salary: 292501, title: 'Senior Engineer', name: 'Allyn Hane', address_street: '411 Kunze Pines', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'liaruel@boyermcglynn.biz', password: 'password'), phone_home: '850.587.2615', bonus: 8780, salary: 92739, title: 'Design Facilitator', name: 'Eleanore Connelly IV', address_street: '8675 Welch Ways', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'rogelio@swaniawski.io', password: 'password'), phone_home: '871.268.5243', bonus: 3887, salary: 116890, title: 'Corporate Marketing Technician', name: 'Miss Santo Bruen', address_street: '5321 Leannon Heights', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'imogenebrown@keeling.co', password: 'password'), phone_home: '599.919.3623', bonus: 7455, salary: 258871, title: 'Central Officer', name: 'Tristan Paucek', address_street: '64180 King Villages', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'oswaldo@pagac.info', password: 'password'), phone_home: '803.079.7250', bonus: 1805, salary: 243551, title: 'Retail Consultant', name: 'Candy Shields', address_street: '998 Adele Radial', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'sanford@rogahn.name', password: 'password'), phone_home: '341.095.6997', bonus: 5081, salary: 181798, title: 'Central Executive', name: 'Dirk Wolff', address_street: '475 Mayer Pike', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'chloeolson@sengerreinger.io', password: 'password'), phone_home: '174.131.6298', bonus: 4371, salary: 101306, title: 'National Advertising Liaison', name: 'Rolf Homenick', address_street: '30395 Cyril Squares', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'millard@moen.net', password: 'password'), phone_home: '647.789.7722', bonus: 6624, salary: 290576, title: 'Hospitality Manager', name: 'Mr. Ardis Greenholt', address_street: '832 Santiago Hollow', role: Role.second, department: dept) 	
end
Department.create!(company: Company.first, name: "Retail").tap do |dept|
	Employee.create!(user: User.new(email: 'gaylene@lebsack.net', password: 'password'), phone_home: '429.234.8308', bonus: 3062, salary: 139263, title: 'Technology Coordinator', name: 'Frieda Bartell', address_street: '23207 Katrina Drive', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'randal@lowe.info', password: 'password'), phone_home: '704.347.5181', bonus: 5433, salary: 224670, title: 'Global Director', name: 'Fidela Kuhlman', address_street: '6059 Funk Creek', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'graig@bashirian.co', password: 'password'), phone_home: '665.051.8586', bonus: 7022, salary: 280357, title: 'Retail Architect', name: 'Armida Nader', address_street: '557 Rosendo Stravenue', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'moshe@dach.net', password: 'password'), phone_home: '019.994.3049', bonus: 9848, salary: 118180, title: 'Principal Community-Services Administrator', name: 'Eugenie Mante', address_street: '64556 Frami Fort', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'karma@wilderman.name', password: 'password'), phone_home: '105.584.9257', bonus: 6024, salary: 54412, title: 'Senior Producer', name: 'Ms. Lonnie Simonis', address_street: '75349 Bradtke Manors', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'amberlybarrows@pagacschmitt.info', password: 'password'), phone_home: '347.950.6026', bonus: 7282, salary: 259268, title: 'Banking Manager', name: 'Oscar Grant', address_street: '76244 Gaynell Fort', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'tillie@hamill.name', password: 'password'), phone_home: '849.763.0991', bonus: 3332, salary: 190428, title: 'Mining Designer', name: 'Hans Howe', address_street: '1920 Allison Plains', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'ilda@hermannjohns.co', password: 'password'), phone_home: '020.199.0522', bonus: 7603, salary: 131278, title: 'Technology Executive', name: 'Roland Zulauf', address_street: '204 Moen Freeway', role: Role.second, department: dept) 	
end
Department.create!(company: Company.first, name: "Real-Estate").tap do |dept|
	Employee.create!(user: User.new(email: 'sanjuana@corkery.biz', password: 'password'), phone_home: '168.986.9254', bonus: 5967, salary: 147531, title: 'Farming Supervisor', name: 'Chris Metz DVM', address_street: '301 Heriberto Stream', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'ronernser@damorebarrows.org', password: 'password'), phone_home: '385.015.0340', bonus: 4971, salary: 48523, title: 'Direct Construction Engineer', name: 'Marivel Predovic', address_street: '643 Theresa Lodge', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'angelesrath@cronin.io', password: 'password'), phone_home: '333.373.0722', bonus: 9687, salary: 158661, title: 'Senior Sales Supervisor', name: 'Asa Krajcik Jr.', address_street: '83741 Lubowitz Ferry', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'angila@waelchiking.info', password: 'password'), phone_home: '213.374.9632', bonus: 9981, salary: 190050, title: 'Regional Construction Technician', name: 'Reyes Mitchell', address_street: '5988 Lebsack Square', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'edward@ritchie.biz', password: 'password'), phone_home: '058.168.0906', bonus: 8981, salary: 89042, title: 'Investor Government Consultant', name: 'Mrs. Susy Wuckert', address_street: '20476 Cremin Brook', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'jordan@okeefe.name', password: 'password'), phone_home: '260.664.4301', bonus: 2952, salary: 178324, title: 'Corporate Banking Representative', name: 'Dion Ledner', address_street: '842 Berge Islands', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'hannelore@murazikquigley.org', password: 'password'), phone_home: '002.234.6277', bonus: 3200, salary: 294615, title: 'Community-Services Supervisor', name: 'Calvin Runolfsson', address_street: '96049 Raymond Centers', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'pasquale@cummerata.name', password: 'password'), phone_home: '843.728.8707', bonus: 4868, salary: 176950, title: 'Consulting Representative', name: 'Ms. Rodney Terry', address_street: '2509 Zemlak Road', role: Role.first, department: dept) 
end

Department.create!(company: Company.second, name: "Mining").tap do |dept|
	Employee.create!(user: User.new(email: 'latriceweinat@fisher.info', password: 'password'), phone_home: '726.157.6718', bonus: 3968, salary: 137874, title: 'Direct Retail Coordinator', name: 'Azzie Jacobson', address_street: '5980 Crist Crossing', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'glen@feest.org', password: 'password'), phone_home: '342.002.6922', bonus: 9317, salary: 247829, title: 'Government Developer', name: 'Monet Weimann', address_street: '664 Wolf Heights', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'cristal@ledner.org', password: 'password'), phone_home: '138.573.7251', bonus: 4022, salary: 65297, title: 'Marketing Engineer', name: 'Percy Feest', address_street: '47907 Turner Haven', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'heriberto@altenwerth.net', password: 'password'), phone_home: '836.567.6226', bonus: 1353, salary: 216657, title: 'Principal Consultant', name: 'Geoffrey Brekke', address_street: '90144 Thiel Station', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'jesusita@sanfordwilderman.io', password: 'password'), phone_home: '624.327.4690', bonus: 8956, salary: 79009, title: 'Lead Banking Executive', name: 'Collen Corwin IV', address_street: '9852 Terrence Terrace', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'lorenarodriguez@stiedemannboyle.info', password: 'password'), phone_home: '577.003.9125', bonus: 1543, salary: 124388, title: 'Mining Officer', name: 'Migdalia Parisian', address_street: '10801 Kuhn Ferry', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'evanstroman@fritschlehner.biz', password: 'password'), phone_home: '513.326.9915', bonus: 9946, salary: 206068, title: 'Lead Healthcare Analyst', name: 'Catherina Oberbrunner', address_street: '141 Chung Common', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'lesaschroeder@denesik.com', password: 'password'), phone_home: '151.610.3939', bonus: 1440, salary: 213169, title: 'Government Director', name: 'Ulysses Hintz', address_street: '6370 Stanton Port', role: Role.second, department: dept) 
end
Department.create!(company: Company.second, name: "Retail").tap do |dept|
	Employee.create!(user: User.new(email: 'adam@brakus.org', password: 'password'), phone_home: '553.214.9076', bonus: 8475, salary: 31495, title: 'Legacy Manufacturing Officer', name: 'Sherie Pollich', address_street: '670 Alverta Trafficway', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'melynda@keeling.biz', password: 'password'), phone_home: '019.520.0935', bonus: 5879, salary: 34006, title: 'Dynamic Farming Officer', name: 'Alvaro Renner II', address_street: '965 Leora Union', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'vonnie@oreilly.biz', password: 'password'), phone_home: '534.393.3352', bonus: 4858, salary: 147771, title: 'Consulting Analyst', name: 'Cindie Kovacek', address_street: '750 Ardella Fords', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'xavierpredovic@heelgutkowski.co', password: 'password'), phone_home: '893.300.0490', bonus: 1170, salary: 97043, title: 'Lead Liaison', name: 'Floyd Emmerich', address_street: '67248 Kling Centers', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'marcus@bergstrom.com', password: 'password'), phone_home: '512.875.0095', bonus: 5764, salary: 181173, title: 'International Farming Director', name: 'Herbert Reichert', address_street: '23943 Bergnaum Inlet', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'monserrate@hauck.com', password: 'password'), phone_home: '940.998.0961', bonus: 5965, salary: 40298, title: 'Forward Technology Planner', name: 'Yee Langosh', address_street: '65978 Runolfsdottir Inlet', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'fernjones@leffler.com', password: 'password'), phone_home: '182.657.7840', bonus: 2340, salary: 255650, title: 'Investor Officer', name: 'Belia Maggio I', address_street: '79693 Ratke Squares', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'sharonda@jaskolski.co', password: 'password'), phone_home: '633.540.8573', bonus: 4740, salary: 272366, title: 'Legacy Community-Services Manager', name: 'Sandy Dietrich V', address_street: '579 Feil Meadows', role: Role.first, department: dept) 
end
Department.create!(company: Company.second, name: "Real-Estate").tap do |dept|
	Employee.create!(user: User.new(email: 'booker@turner.io', password: 'password'), phone_home: '556.167.9322', bonus: 4672, salary: 204793, title: 'Legal Supervisor', name: 'Alix Mosciski IV', address_street: '30266 Flavia Estate', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'alva@schoenleannon.name', password: 'password'), phone_home: '972.423.5707', bonus: 1025, salary: 229188, title: 'Product Mining Analyst', name: 'Jewel Metz', address_street: '333 Bashirian Avenue', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'armando@leuschke.com', password: 'password'), phone_home: '461.635.7670', bonus: 1266, salary: 188286, title: 'International Education Designer', name: 'Aron Torp', address_street: '790 Johnson Mountains', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'forrest@runolfsdottir.info', password: 'password'), phone_home: '105.560.8154', bonus: 5755, salary: 73259, title: 'Marketing Orchestrator', name: 'Delcie VonRueden', address_street: '1551 Abraham Ferry', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'noelle@fritsch.com', password: 'password'), phone_home: '412.352.2129', bonus: 1041, salary: 220670, title: 'Corporate Facilitator', name: 'Mr. Oren Rutherford', address_street: '69813 Bernie View', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'eusebio@nader.name', password: 'password'), phone_home: '228.697.7163', bonus: 4430, salary: 54806, title: 'Administration Specialist', name: 'Miss Bryon Schuppe', address_street: '710 Ebert Streets', role: Role.second, department: dept) 
	Employee.create!(user: User.new(email: 'kelly@faheymccullough.com', password: 'password'), phone_home: '956.348.5765', bonus: 4077, salary: 36488, title: 'Healthcare Orchestrator', name: 'Ora Tillman', address_street: '75701 Towne Place', role: Role.first, department: dept) 
	Employee.create!(user: User.new(email: 'sachiko@spinka.co', password: 'password'), phone_home: '943.500.9098', bonus: 9329, salary: 224749, title: 'Principal Engineer', name: 'Boyd Ratke I', address_street: '9139 Eladia Shore', role: Role.second, department: dept) 
end

